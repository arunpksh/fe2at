#!/bin/env python
# /*****************************************************************************\
# *                                                                             *
# *                               - F E 2 A T -                                 *
# *                                                                             *
# * Program to map FE displacements onto Atoms                                  *
# * Copyright (C) 2011-2016                                                     *
# * Authors: J.J. Moeller, A. Prakash & E. Bitzek                               *
# * (Friedrich-Alexander-Universitaet Erlangen-Nuernberg (FAU), Germany)        *
# *                                                                             *
# * This program is free software: you can redistribute it and/or modify        *
# * it under the terms of the GNU General Public License as published by        *
# * the Free Software Foundation, either version 3 of the License, or           *
# * (at your option) any later version.                                         *
# *                                                                             *
# * This program is distributed in the hope that it will be useful,             *
# * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
# * GNU General Public License for more details.                                *
# *                                                                             *
# * You should have received a copy of the GNU General Public License           *
# * along with this program.  If not, see <http://www.gnu.org/licenses/>        *
# *                                                                             *
# *                                                                             *
#----------------------------------                                             *
# * Simple python script to extract nodal displacements from a FE simulation    *
# * Author: A. Prakash                                                          *
# *                                                                             *
# * For more details visit the wiki of FE2AT                                    *
# * https://bitbucket.org/arunpksh/fe2at/wiki/Home                              *
# *                                                                             *
# \*****************************************************************************/
#
# Usage:
# abaqus python ./extractDisp.py [Jobname]

from odbAccess import *
import sys

def main(jobName):
  odb = openOdb(jobName+'.odb')
  fName = 'FEdisp'
  for stepName, step in odb.steps.items():
    print 'Working on Step', stepName
    for frame in step.frames:
      fData = open(fName+str(frame.frameId)+'.dat','w+')
      fData.write("Node \t Disp \n")
      fv = frame.fieldOutputs['U']
      for val in fv.values:
	nodeLabel = val.nodeLabel
	nodeData  = val.data
	outStringData = ""
	for aVal in nodeData:
	  outStringData += "%15.4e" %(aVal)
	outString = "%10i \t " %(nodeLabel)
	fData.write(outString+outStringData+"\n")

if __name__=="__main__":
  jobName = sys.argv[1]
  sys.exit(main(jobName))
	
