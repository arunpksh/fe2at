! /*****************************************************************************\
! *                                                                         	*
! *                               - F E 2 A T -                             	*
! *                                                                           	*
! * Program to map FE displacements onto Atoms				     	*
! * Copyright (C) 2011-2016                                                     *
! * Authors: J.J. Moeller, A. Prakash & E. Bitzek                               *
! * (Friedrich-Alexander-Universitaet Erlangen-Nuernberg (FAU), Germany)        *
! *                                                                             *
! * This program is free software: you can redistribute it and/or modify	*
! * it under the terms of the GNU General Public License as published by	*
! * the Free Software Foundation, either version 3 of the License, or		*
! * (at your option) any later version.						*
! * 										*
! * This program is distributed in the hope that it will be useful,		*
! * but WITHOUT ANY WARRANTY; without even the implied warranty of		*
! * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the		*
! * GNU General Public License for more details.                                *
! *                                                                             *
! * You should have received a copy of the GNU General Public License           *
! * along with this program.  If not, see <http://www.gnu.org/licenses/>        *
! *                                                                             *
! *                                                                             *
! * NOTE: Parts of the code are licensed under LGPL. Please see the relevant... *
! * ... parts for more details                                                  *
!----------------------------------                                             *
! * Old version: 02.01.12						     	*
! * Today: Thu May  3 08:56:16 CEST 2012					*
! *                                                                           	*
! \*****************************************************************************/


!	/////////////////////////////////////////
!	/				     	/
!	/ The module 'FE2ATsize' provides    	/
!	/                                       /
!	/                                       /
!	/                                       /
!	///////////////////////////////////////// 


MODULE FE2ATsize
  implicit none
 
  ! Memory allocation seems to be causing problems
  ! Reading only into one DATAARR and copying relevant data 
  ! onto dynamically allocated arrays may be useful
  integer, parameter :: nColsInFileMAX = 10
  integer, parameter :: nDataLinesMAX = 100000000
  
  CONTAINS
     
! Computation of length of 3x1 vector:

  real*8 FUNCTION norm31(p)
    implicit none
    real*8 p(3)
    
    norm31 = sqrt(p(1)**2.d0+p(2)**2.d0+p(3)**2.d0)
    return
  END FUNCTION norm31

! Computation of cross product of two 3x1 vectors:
  
  FUNCTION cross31(a,b) result(vec31)
    implicit none
    real*8, dimension(3) :: a,b,vec31
    
    vec31(1) = a(2) * b(3) - a(3) * b(2)
    vec31(2) = a(3) * b(1) - a(1) * b(3)
    vec31(3) = a(1) * b(2) - a(2) * b(1)
    return
  END FUNCTION cross31
  
! Inversion of 3x3 matrices:

  SUBROUTINE invert33 (A, Ainv)
    implicit none
    real*8, intent(in), dimension(3,3) :: A
    real*8, intent(out), dimension(3,3) :: Ainv
    real*8  Adet    

    Ainv = 0d+0;
    Ainv(1,1) = A(2,2)*A(3,3) - A(3,2)*A(2,3)
    Ainv(1,2) = A(3,2)*A(1,3) - A(1,2)*A(3,3)
    Ainv(1,3) = A(1,2)*A(2,3) - A(1,3)*A(2,2)
    Adet = Ainv(1,1)*A(1,1) + Ainv(1,2)*A(2,1) + Ainv(1,3)*A(3,1)
    Ainv(1,1) = Ainv(1,1)/Adet
    Ainv(1,2) = Ainv(1,2)/Adet
    Ainv(1,3) = Ainv(1,3)/Adet
    Ainv(2,1) = (A(2,3)*A(3,1) - A(2,1)*A(3,3))/Adet
    Ainv(2,2) = (A(1,1)*A(3,3) - A(3,1)*A(1,3))/Adet
    Ainv(2,3) = (A(2,1)*A(1,3) - A(1,1)*A(2,3))/Adet
    Ainv(3,1) = (A(2,1)*A(3,2) - A(2,2)*A(3,1))/Adet
    Ainv(3,2) = (A(3,1)*A(1,2) - A(1,1)*A(3,2))/Adet
    Ainv(3,3) = (A(1,1)*A(2,2) - A(1,2)*A(2,1))/Adet
    return     
  END SUBROUTINE invert33

END MODULE FE2ATsize

!	/////////////////////////////////////////
!	/                                    	/
!	/ The module 'qsort_c_module' is a   	/
!	/ recursive F95 quicksort routine.   	/
!	/ It sorts real numbers into         	/
!	/ ascending numerical order	     	/
!	/                                    	/
!	/ Author: Juli Rew, SCD Consulting   	/
!	/ (juliana@ucar.edu), 9/03	     	/
! 	/ Based on algorithm from		/
!	/ Cormen et al., Introduction to	/
!	/ Algorithms, 1997 		  	/
!	/					/
!	/ Made F conformant by Walt Brainerd	/
!	/					/
!       /////////////////////////////////////////

MODULE qsort_c_module
  implicit none
  public :: QsortC
  private :: Partition

  CONTAINS

  RECURSIVE SUBROUTINE QsortC(A)
    real*8, intent(in out), dimension(:,:) :: A
    integer :: iq

    if(size(A,1) > 1) then
      call Partition(A, iq)
      call QsortC(A(:iq-1,:))
      call QsortC(A(iq:,:))
    endif
  END SUBROUTINE QsortC

  SUBROUTINE Partition(A, marker)
    real*8, intent(in out), dimension(:,:) :: A
    integer, intent(out) :: marker
    integer :: i, j
    real*8 :: temp,tmp
    real*8 :: x      ! pivot point
    x = A(1,2)
    i = 0
    j = size(A,1) + 1

    do
      j = j-1
      do
        if (A(j,2) <= x) exit
        j = j-1
      end do
      i = i+1
      do
        if (A(i,2) >= x) exit
        i = i+1
      end do
      if (i < j) then
        ! exchange A(i) and A(j)
        temp = A(i,2)
        tmp  = A(i,1)
        A(i,2) = A(j,2)
        A(i,1) = A(j,1)
        A(j,2) = temp
        A(j,1) = tmp
      elseif (i == j) then
        marker = i+1
        return
      else
        marker = i
        return
      endif
    end do
  END SUBROUTINE Partition

END MODULE qsort_c_module

!       /////////////////////////////////////////
!	/					/
! 	/ Definition of input files format and 	/
!	/ storage in corresponding arrays	/
!       /                                       /
!       /////////////////////////////////////////
  
SUBROUTINE FileRead(fname,nColsInFile,dataArr,nDataLines)
  USE FE2ATsize
  implicit none
  character*80 fname
  character (len=300) :: line,nline
  integer nColsInFile,nDataLines,i,j,k,next_space,first_dot,next_comma,next
  real*8 dataArr(nDataLinesMAX,nColsInFileMAX)
  real*8 a,b,c,d,e,f,g,h
  next = 0
  next_comma = 0
  next_space = 0
  nColsInFile = 0
  open(unit=500,file=trim(fname),status='old',err=101)

  ! Omit comment lines and determine nColsInFile:
  do
    read (500,'(A)') line
    if ((line(1:1) /= "#") .AND. (line(1:1) /= "*")) then  
      do
        nline = trim(adjustl(line(:)))                        ! trim the leading and trailing spaces
        select case (iachar(nline(:1)))                       ! check what is written as first character in line
          case (45,46,48:57)                                  ! if it's '.','-' or 0...9 in ASCII format,
          nColsInFile = nColsInFile+1                         ! increase the counter.
        end select 
        next_comma = index(trim(nline),',')
        next_space = index(trim(nline),' ')
        if (next_comma > next_space) then
          next = next_space
        else 
          next = next_space
        end if
        line = adjustl(nline(next+1:))
        if (next_comma .eq. next_space) then 
          write(*,*)'nColsInFile: ', nColsInFile
          exit
        end if
      end do
      exit
    end if
  end do 

  if(nColsInFile>nColsInFileMAX) then
    write(*,*)'Error in FileRead: nColsInFile>nColsInFileMAX'
    stop
  endif

  backspace (500)
!  write(*,*)'BLABLA in FileRead:',line

  ! Read data lines and store into data array:
  i = 1
  100 read(500,*,end=102)(dataArr(i,j),j=1,nColsInFile)
  i = i+1
!  write(*,*)'i:',i
  go to 100

  ! Check if actual data lines exceeds the defined maximum number of data lines:
  102 nDataLines = i-1

  if (nDataLines > nDataLinesMAX) then
    write(*,*)'ERROR in FileRead: nDataLines>nDataLinesMAX'
    stop
  endif

  close(500)
  return

  ! If any error occurred during reading:
  101 write(*,*)'Error reading file - ',trim(fname)
  write(*,*)'Ensure only data lines are present in the file'
  write(*,*)'Comment lines must only be in the beginning of the file...'
  write(*,*)'... and must start with a # '
  write(*,*)'ERROR - Stopping Program!!!'
  stop

END SUBROUTINE FileRead

!       /////////////////////////////////////////
!       /                                       /
! 	/ Computation of distance from actual 	/
!	/ atom to nodes				/
!       /                                       /
!       /////////////////////////////////////////

   
SUBROUTINE calcDistanceToNodes(NodalCoords,atomCoord,nNodes,DistToNodes)
  USE FE2ATsize
  implicit none
    real*8 NodalCoords(nDataLinesMAX,nColsInFileMAX)
    real*8 atomCoord(3)
    integer nNodalCols,nNodes
    real*8 DistToNodes(nNodes,2),localDataArr(4)
    integer i,j,k
    
    do i=1,nNodes
      localDataArr(1:4) = NodalCoords(i,1:4)
      DistToNodes(i,1) = localDataArr(1)
      DistToNodes(i,2) = sqrt((localDataArr(2)-atomCoord(1))**2.d0+ &
                              (localDataArr(3)-atomCoord(2))**2.d0+ &
                              (localDataArr(4)-atomCoord(3))**2.d0)
    enddo
  return
END SUBROUTINE calcDistanceToNodes

!       /////////////////////////////////////////
!       /                                       /
!       / SOME STUFF FOR POINT-LOCATION IN FE   /
!       / MESH, see Krause, Rank, Computing 57, /
!	/ p. 49-62 (1996)			/
!       /                                       /
!       /////////////////////////////////////////

MODULE POLO
  implicit none

  TYPE treenode
    real*8 :: cell(2,3)
    integer, pointer :: first => null()
    integer id 
!    logical :: leaf = .FALSE.
    type(treenode), pointer :: child1 => null()
    type(treenode), pointer :: child2 => null()
    type(treenode), pointer :: child3 => null()
    type(treenode), pointer :: child4 => null()
    type(treenode), pointer :: child5 => null()
    type(treenode), pointer :: child6 => null()
    type(treenode), pointer :: child7 => null()
    type(treenode), pointer :: child8 => null()
  END TYPE
  
  type(treenode), pointer :: tree => null()
  integer :: counter = 1
  logical found

  CONTAINS

! Creating the octree (in 3D):

  RECURSIVE SUBROUTINE BuildTree (tree,cell,ElementCenters,Elements,ElementSortedList,nElements,first)
    implicit none
    type (treenode), pointer :: tree,tmp                          ! A tree
    real*8, intent(in) :: cell(:,:)
    integer, target :: Elements(:,:)
    real, target :: ElementCenters(:,:)
    integer id,nChilds,i,j,k,m,nElements,nElCell
    real*8 newcell(2,3)
    integer, pointer :: first
    integer, target :: ElementSortedList(:,:)

    nElCell = 0
!    write(*,'(a,6(F5.2,1X),a,I3)') 'search through all elements in cell ', cell,', starting with', first
    
    do i=first,nElements
      if (ElementCenters(i,1)<cell(1,1) .or. ElementCenters(i,1)>=cell(2,1) .or.&
          ElementCenters(i,2)<cell(1,2) .or. ElementCenters(i,2)>=cell(2,2) .or. &
          ElementCenters(i,3)<cell(1,3) .or. ElementCenters(i,3)>=cell(2,3) ) then
      else
!        write(*,'(a,3(1X,F5.2)$)')'Element with center (', ElementCenters(i,:)
!        write(*,'(a,2(1X,F5.2),2(1X,F5.2),2(1X,F5.2))') ') is in cell ', cell 
        nElCell = nElCell + 1
        first => Elements(i,1)
!        write(*,*)'counter: ',counter
      end if
    end do

    if (nElCell == 0) then
      return
    else
      allocate(tree)
      tree%cell(:,:) = cell(:,:)
      tree%id = counter
      ElementSortedList(1,counter) = tree%id
      ElementSortedList(2:,counter) = Elements(first,:)
      tree%first => ElementSortedList(2,counter)
    end if

!    tree%nChilds = 0
!    if (counter == 1) then
!      tree%id = counter
!      ElementSortedList(1,1) = first
!      ElementSortedList(2:,1) = Elements(first,:)
!      tree%first => ElementSortedList(2,1)
!    end if

    if (nElCell == 1) then
!      ElementSortedList(1,counter)=counter
!      ElementSortedList(2:,counter)=Elements(first,:)
!      tree%first => ElementSortedList(2,counter)
!      tree%id = counter
!      tree%leaf=.true.
!      write(*,*)ElementSortedList(:,counter)
!      write(*,'(a,I3,a,6(1X,F5.2))')'Element ',first,' uniquely defined to be in cell', cell
      counter = counter + 1
      return  
    end if

!    write(*,'(I3,a)')nElCell,' elements found to be in cell, thus divide into 8 new sub-cells'
    ! Calculate new dimensions of subcells
    nChilds = 0
    do i=0,1
      do j=0,1
        do k=0,1
          nChilds = nChilds + 1

          ! compute dimensions of new sub-cell
          newcell=reshape((/cell(1,1)+i*((cell(2,1)-cell(1,1))/2),cell(2,1)-(1-i)*((cell(2,1)-cell(1,1))/2),&
                            cell(1,2)+j*((cell(2,2)-cell(1,2))/2),cell(2,2)-(1-j)*((cell(2,2)-cell(1,2))/2),&
                            cell(1,3)+k*((cell(2,3)-cell(1,3))/2),cell(2,3)-(1-k)*((cell(2,3)-cell(1,3))/2)/),&
                         (/2,3/)) 
          !write(*,'((2(1X,F5.2)))')newcell

          ! determine first element in cell
          do m=1,nElements
            if (ElementCenters(m,1)>=newcell(1,1) .and. ElementCenters(m,1)<newcell(2,1) .and.&
                ElementCenters(m,2)>=newcell(1,2) .and. ElementCenters(m,2)<newcell(2,2) .and. &
                ElementCenters(m,3)>=newcell(1,3) .and. ElementCenters(m,3)<newcell(2,3) ) then
!              write(*,'(a,6(F5.2,1X),a,I3)')'first element of subcell ', newcell,' is element Nr.',m
              first => Elements(m,1)
              goto 150
            end if
          end do
!          write(*,*)'child of the cell ',tree%id,nChilds

          ! call BuildTree recursively...
150       select case (nChilds)
            case (1) 
              call BuildTree (tree%child1,newcell,ElementCenters,Elements,ElementSortedList,nElements,first)
            case (2) 
              call BuildTree (tree%child2,newcell,ElementCenters,Elements,ElementSortedList,nElements,first)
            case (3) 
              call BuildTree (tree%child3,newcell,ElementCenters,Elements,ElementSortedList,nElements,first)
            case (4) 
              call BuildTree (tree%child4,newcell,ElementCenters,Elements,ElementSortedList,nElements,first)
            case (5) 
              call BuildTree (tree%child5,newcell,ElementCenters,Elements,ElementSortedList,nElements,first)
            case (6) 
              call BuildTree (tree%child6,newcell,ElementCenters,Elements,ElementSortedList,nElements,first)
            case (7) 
              call BuildTree (tree%child7,newcell,ElementCenters,Elements,ElementSortedList,nElements,first)
            case (8) 
              call BuildTree (tree%child8,newcell,ElementCenters,Elements,ElementSortedList,nElements,first)
          end select 
!          call BuildTree (tmp,newcell,ElementCenters,ElementSortedList,nElements,first)
!          write(*,*)'-------'
!          return
        end do
      end do
    end do

  END SUBROUTINE BuildTree

  RECURSIVE SUBROUTINE SearchPoint (tree,tar,ElementCenters,el)
    real*8 tar(3)
    type (treenode), pointer :: tree ! A tree
    real ElementCenters(:,:)
    integer el,nElCell

    if (found .EQ. .TRUE.) then
!      write(*,*)'already found, returning...'
      return
    else
!      write(*,'(a,3(F5.2,1X),a,I3,a$)')'Searching for (', tar,') in cell (', tree%id,')'
!      write(*,*)tree%leaf
      if (tar(1)>=tree%cell(1,1) .and. tar(1)<tree%cell(2,1) .and. &
          tar(2)>=tree%cell(1,2) .and. tar(2)<tree%cell(2,2) .and. &
          tar(3)>=tree%cell(1,3) .and. tar(3)<tree%cell(2,3)) then
        if (associated(tree%child1) .and. found /= .TRUE.) then
          nElCell = nElCell + 1
          call SearchPoint(tree%child1,tar,ElementCenters,el)
        end if
        if (associated(tree%child2) .and. found /= .TRUE.) then
          nElCell = nElCell + 1
          call SearchPoint(tree%child2,tar,ElementCenters,el)
        end if
        if (associated(tree%child3) .and. found /= .TRUE.) then
          nElCell = nElCell + 1
          call SearchPoint(tree%child3,tar,ElementCenters,el)
        end if
        if (associated(tree%child4) .and. found /= .TRUE.) then
          nElCell = nElCell + 1
          call SearchPoint(tree%child4,tar,ElementCenters,el)
        end if
        if (associated(tree%child5) .and. found /= .TRUE.) then
          nElCell = nElCell + 1
          call SearchPoint(tree%child5,tar,ElementCenters,el)
        end if
        if (associated(tree%child6) .and. found /= .TRUE.) then
          nElCell = nElCell + 1
          call SearchPoint(tree%child6,tar,ElementCenters,el)
        end if
        if (associated(tree%child7) .and. found /= .TRUE.) then
          nElCell = nElCell + 1
          call SearchPoint(tree%child7,tar,ElementCenters,el)
        end if
        if (associated(tree%child8) .and. found /= .TRUE.) then
          nElCell = nElCell + 1
          call SearchPoint(tree%child8,tar,ElementCenters,el)
        end if
!        write(*,'(a,6(F5.2,1X),a)')'Ultimately found in cell (',tree%cell ,')' 
!        write(*,'(a,I3,a,3(F5.2,1X),a)')'Maybe the point is in element Nr. ',tree%first,' (',ElementCenters(tree%first,:),')' 
        if (found == .FALSE.) then
!          write(*,*)'found  target (',tar,') in cell id ',tree%id,tree%leaf,nElCell
          el = tree%id
          found = .TRUE.
          return        
        end if
      else   
!        write(*,*)'not inside tree id ',tree%id
        return
      end if
    end if
!      else if (found == .FALSE.) then 
!        write(*,'(a,I3)')'no match found in ', tree%id
!      end if

    END SUBROUTINE SearchPoint 

! Calculation of center of mass and storage for each element
! Storage of pointers to nodes: 
! [ center_x, center_y, center_z ]

  SUBROUTINE CalcCenters (ElementsIn,ElementCenters,nCols,Nodes)
    implicit none
    integer nCols,nElements,node,i,j
    integer, dimension(2) :: lenArr
    integer ElementsIn(:,:)
    real*8 Nodes(:,:)
    real ElementCenters(:,:)
    real*8 centx,centy,centz

    lenArr(:) = shape(ElementCenters)
    nElements = lenArr(1)

    ! First, calculate center of mass for every element:
    do i=1,nElements
      centx=0;centy=0;centz=0;
      do j=2,nCols
        node = int(ElementsIn(i,j))
        centx = centx + Nodes(node,2)
        centy = centy + Nodes(node,3)
        centz = centz + Nodes(node,4)
      end do
      ElementCenters(i,1) = centx / (nCols-1)
      ElementCenters(i,2) = centy / (nCols-1)
      ElementCenters(i,3) = centz / (nCols-1)
    end do
  
  END SUBROUTINE CalcCenters

  SUBROUTINE GetNeighbors2 (Elements,ElementCentersTransp,Neighbors,nElements)
    implicit none
    integer Elements(:,:) 
    real ElementCentersTransp(:,:)
    real, dimension(nElements) :: ElementDist
    integer :: Neighbors(:,:)
    integer nElements
    integer i,j,k,l,el,tmpel,marker
    real qdist,tmp
!    write(*,*)shape(Neighbors)
    Neighbors = 0
 
!    write(*,'(a$)') 'working on element '
!    write(*,*)ElementDist(:,7)
    do i=1,nElements
      ElementDist = 0
!      write(*,'($)'),i
      do j=1,nElements
!        write(*,*)'----------------------------------'
!        write(*,*)'calculating the distance to Nr.',j
!        write(*,*)i,ElementCentersTransp(1:3,i)
!        write(*,*)j,ElementCentersTransp(1:3,j)
        el=j
        qdist=abs(ElementCentersTransp(1,i)-ElementCentersTransp(1,j)) + &
              abs(ElementCentersTransp(2,i)-ElementCentersTransp(2,j)) + &
              abs(ElementCentersTransp(3,i)-ElementCentersTransp(3,j))
!        write(*,*)'for element nr.',el , ' the distance is ',qdist
        do k=1,nElements
          if (qdist .le. ElementDist(k)) then
!            write(*,*) 'inserting ',qdist , ' instead of ',ElementDist(k)
            marker=k
            exit
          else if (qdist .gt. ElementDist(k) .and. qdist .le. ElementDist(k+1)) then
!            write(*,*) 'inserting ',qdist , ' between ',ElementDist(k), ' and ' ,  ElementDist(k+1)
            marker=k+1
            exit
          else if (qdist .gt. ElementDist(k) .and. ElementDist(k+1) == 0) then
            marker=k+1
            exit
          end if
        end do
        do l=marker,nElements
          if (Neighbors(l,i) == 0) then
            ElementDist(l)=qdist
            Neighbors(l,i)=el
            exit
          else
!            write(*,*) 'swapping ',qdist , ' with ', ElementDist(marker)
            tmp=ElementDist(l)
            tmpel=Neighbors(l,i)
            ElementDist(l)=qdist
            Neighbors(l,i)=el
            qdist=tmp
            el=tmpel
          end if
        end do
      end do
    end do
!    write(*,*)'ElementDist:',ElementDist(:,45)
!    write(*,*)'Neighbors:',Neighbors(:,45)


  END SUBROUTINE GetNeighbors2


! Determination of all neighbors for all elements 
! (prevents the later re-and-re-and-re-calculation) 
! The structure of Neighbors(:,:) is, as follows:
! [ num_neighbors, neighbor1, neighbor2, neighbor3, neighbor4, ...]

  SUBROUTINE GetNeighbors (Elements,Nodes,Neighbors,nNPE,maxNeighbors,nElements,nNodes)
    implicit none
    real*8 Elements(:,:), Nodes(:,:)
    integer, dimension(nNodes,50) :: NeighborList
    integer, dimension(nElements,nElements) :: flaglist
    integer nNPE,i,j,k,nElements,nNodes,NodesCount(nNodes),node,el,Neighbors(:,:),maxNeighbors

    NodesCount(:) = 0
    flaglist(:,:) = 0
    Neighbors(:,:) = 0
    NeighborList(:,:) = 0

! Loop for determination of all elements sharing the same node
! [ num_nodeSharingElements, element1, element2, element3, element4, ... ]

    do j=2,nNPE+1
      do i=1,nElements
        node = Elements(i,j) 
        NeighborList(node,1)=NeighborList(node,1)+1
        NeighborList(node,NeighborList(node,1)+1) = Elements(i,1)
      end do
    end do
    write(*,'(a,20(I3,1X))')'Neighborlist:',NeighborList(:,:)
! Determination of all neighbors for all elements 
! (prevents the later re-and-re-and-re-calculation) 
! The structure of Neighbors(:,:) is, as follows:
! [ num_neighbors, neighbor1, neighbor2, neighbor3, neighbor4, ...]

    do j=2,nNPE+1
      do i=1,nElements
         flaglist(i,i) = 1
         node = Elements(i,j)
         do k=2,NeighborList(node,1)
           el = NeighborList(node,k)  
           if (flaglist(i,el) .ne. 1) then
             flaglist(i,el) = 1
             Neighbors(i,1) = Neighbors(i,1) + 1
             Neighbors(i,Neighbors(i,1)+1) = el
           end if
         end do 
      end do
    end do    
    write(*,*)'flaglist for el 1:',NeighborList(1,:)

  END SUBROUTINE GetNeighbors

  SUBROUTINE checkZeroNodes(v,node_num,el,ElementCenters)
    implicit none
    real*8, dimension(:,:) :: v 
    real ElementCenters(:,:)
    integer node_num,j,i,vzero(3),el

    vzero = 0    
   
    if (maxval(v(:,:)) < 0 .or. minval(v(:,:)) > 0 ) return 

    do j=1,3
      if (ElementCenters(el,j) > 0) then
        vzero(j) = 1 
      else
        vzero(j) = -1
      end if
    end do

    do i=1,3
      do j=1,node_num
        if (v(i,j) == 0) then
          if (vzero(i) == 1) then
            v(i,j) = -0.1
          else
            v(i,j) = 0.1
          end if
        end if
      end do
    end do

  END SUBROUTINE checkZeroNodes

END MODULE POLO

!       /////////////////////////////////////////
!       /                                       /
!       / Module mapping written by Arun,       /
!       / maps and interpolates nodal           /
!       / displacements from FE onto            /
!       / the atoms.                            /
!       /                                       /
!       /////////////////////////////////////////

MODULE MAPPING
  implicit none

  CONTAINS
Subroutine calcNaturalCoordsTet(ElementNodalCoords,AtomCoords,NaturalCoords,ShapeFuncVec)
!-----------------------------------------------
!   Subroutine calcNaturalCoordsTet
!      - calculates the natural coordinates of a point in a tetrahedron
!      - The natural coords of the tetrahedron run from 0 to 1
!      - For further operations, keeping in view a general purpose formulation
!        the shape functions are also returned
!      Since the shape functions are of further interest
!-----------------------------------------------
 use FE2ATsize
 implicit none
   real*8, intent(in), dimension(4,3) :: ElementNodalCoords
   real*8, intent(in), dimension(3) :: AtomCoords
   real*8, intent(out), dimension(3) :: NaturalCoords
   real*8, intent(out), dimension(4) :: ShapeFuncVec
   real*8 A(3,3), Ainv(3,3),RHSvec(3)
   
   A(1,1) = ElementNodalCoords(1,1) - ElementNodalCoords(4,1)
   A(1,2) = ElementNodalCoords(2,1) - ElementNodalCoords(4,1)
   A(1,3) = ElementNodalCoords(3,1) - ElementNodalCoords(4,1)
   A(2,1) = ElementNodalCoords(1,2) - ElementNodalCoords(4,2)
   A(2,2) = ElementNodalCoords(2,2) - ElementNodalCoords(4,2)
   A(2,3) = ElementNodalCoords(3,2) - ElementNodalCoords(4,2)
   A(3,1) = ElementNodalCoords(1,3) - ElementNodalCoords(4,3)
   A(3,2) = ElementNodalCoords(2,3) - ElementNodalCoords(4,3)
   A(3,3) = ElementNodalCoords(3,3) - ElementNodalCoords(4,3)
   
   RHSvec(1) = AtomCoords(1) - ElementNodalCoords(4,1)
   RHSvec(2) = AtomCoords(2) - ElementNodalCoords(4,2)
   RHSvec(3) = AtomCoords(3) - ElementNodalCoords(4,3)
   
   CALL invert33(A,Ainv)
   
   NaturalCoords(1) = Ainv(1,1)*RHSvec(1)+Ainv(1,2)*RHSvec(2)+Ainv(1,3)*RHSvec(3)
   NaturalCoords(2) = Ainv(2,1)*RHSvec(1)+Ainv(2,2)*RHSvec(2)+Ainv(2,3)*RHSvec(3)
   NaturalCoords(3) = Ainv(3,1)*RHSvec(1)+Ainv(3,2)*RHSvec(2)+Ainv(3,3)*RHSvec(3)
   
   ShapeFuncVec(1) = NaturalCoords(1)
   ShapeFuncVec(2) = NaturalCoords(2)
   ShapeFuncVec(3) = NaturalCoords(3)
   ShapeFuncVec(4) = 1.d0 - NaturalCoords(1) - NaturalCoords(2) - NaturalCoords(3)

 return
 End Subroutine calcNaturalCoordsTet 



Subroutine calcNaturalCoordsHex(ElementNodalCoords,AtomCoords,NaturalCoords,ShapeFuncVec)
!-----------------------------------------------
!   Subroutine calcNaturalCoordsHex
!      - calculates the natural coordinates of a point in a Hexahedron
!      - The natural coords of the Hexahedron run from -1 to 1
!      - For further operations, keeping in view a general purpose formulation
!        the shape functions are also returned
!      Since the shape functions are of further interest
!-----------------------------------------------
 use FE2ATsize
 implicit none
   real*8, intent(in), dimension(8,3) :: ElementNodalCoords
   real*8, intent(in), dimension(3) :: AtomCoords
   real*8, intent(out), dimension(3) :: NaturalCoords
   real*8, intent(out), dimension(8) :: ShapeFuncVec
   
   real*8 TOL
   logical continueItr
   real*8, dimension(8) :: xc,yc,zc
   real*8, dimension(3) :: CoordL_itr,FuncVec,deltaCoordL_itr
   real*8, dimension(3,3) :: Gmat, Gmatinv
   real*8 errF, errC
   real*8 f0,f1,f2,f3,f4,f5,f6,f7
   real*8 g0,g1,g2,g3,g4,g5,g6,g7
   real*8 h0,h1,h2,h3,h4,h5,h6,h7
   integer itr,maxitr,i,j
   
   xc(1:8) = ElementNodalCoords(1:8,1)
   yc(1:8) = ElementNodalCoords(1:8,2)
   zc(1:8) = ElementNodalCoords(1:8,3)
   
   CoordL_itr(1:3) = 0.d0  !guesstimates
   TOL = 1.e-5
   maxitr = 20
   itr = 0
   continueItr=.true.
   
   do while (continueItr)
     itr=itr+1
     f0 = (xc(8)+xc(7)+xc(6)+xc(5)+xc(4)+xc(3)+xc(2)+xc(1))/8.d0 - AtomCoords(1)
     f1 = (xc(8)-xc(7)+xc(6)-xc(5)+xc(4)-xc(3)+xc(2)-xc(1))/8.d0
     f2 = (xc(8)+xc(7)-xc(6)-xc(5)+xc(4)+xc(3)-xc(2)-xc(1))/8.d0
     f3 = (xc(8)+xc(7)+xc(6)+xc(5)-xc(4)-xc(3)-xc(2)-xc(1))/8.d0
     f4 = (xc(8)-xc(7)-xc(6)+xc(5)+xc(4)-xc(3)-xc(2)+xc(1))/8.d0
     f5 = (xc(8)-xc(7)+xc(6)-xc(5)-xc(4)+xc(3)-xc(2)+xc(1))/8.d0
     f6 = (xc(8)+xc(7)-xc(6)-xc(5)-xc(4)-xc(3)+xc(2)+xc(1))/8.d0
     f7 = (xc(8)-xc(7)-xc(6)+xc(5)-xc(4)+xc(3)+xc(2)-xc(1))/8.d0

!      write(*,*)'in sub'
!      write(*,'(8f5.1)')f0,f1,f2,f3,f4,f5,f6,f7
   
     g0 = (yc(8)+yc(7)+yc(6)+yc(5)+yc(4)+yc(3)+yc(2)+yc(1))/8.d0 - AtomCoords(2)
     g1 = (yc(8)-yc(7)+yc(6)-yc(5)+yc(4)-yc(3)+yc(2)-yc(1))/8.d0
     g2 = (yc(8)+yc(7)-yc(6)-yc(5)+yc(4)+yc(3)-yc(2)-yc(1))/8.d0
     g3 = (yc(8)+yc(7)+yc(6)+yc(5)-yc(4)-yc(3)-yc(2)-yc(1))/8.d0
     g4 = (yc(8)-yc(7)-yc(6)+yc(5)+yc(4)-yc(3)-yc(2)+yc(1))/8.d0
     g5 = (yc(8)-yc(7)+yc(6)-yc(5)-yc(4)+yc(3)-yc(2)+yc(1))/8.d0
     g6 = (yc(8)+yc(7)-yc(6)-yc(5)-yc(4)-yc(3)+yc(2)+yc(1))/8.d0
     g7 = (yc(8)-yc(7)-yc(6)+yc(5)-yc(4)+yc(3)+yc(2)-yc(1))/8.d0
!      write(*,*)'in sub - g'
!      write(*,'(8f5.1)')g0,g1,g2,g3,g4,g5,g6,g7
   
     h0 = (zc(8)+zc(7)+zc(6)+zc(5)+zc(4)+zc(3)+zc(2)+zc(1))/8.d0 - AtomCoords(3)
     h1 = (zc(8)-zc(7)+zc(6)-zc(5)+zc(4)-zc(3)+zc(2)-zc(1))/8.d0
     h2 = (zc(8)+zc(7)-zc(6)-zc(5)+zc(4)+zc(3)-zc(2)-zc(1))/8.d0
     h3 = (zc(8)+zc(7)+zc(6)+zc(5)-zc(4)-zc(3)-zc(2)-zc(1))/8.d0
     h4 = (zc(8)-zc(7)-zc(6)+zc(5)+zc(4)-zc(3)-zc(2)+zc(1))/8.d0
     h5 = (zc(8)-zc(7)+zc(6)-zc(5)-zc(4)+zc(3)-zc(2)+zc(1))/8.d0
     h6 = (zc(8)+zc(7)-zc(6)-zc(5)-zc(4)-zc(3)+zc(2)+zc(1))/8.d0
     h7 = (zc(8)-zc(7)-zc(6)+zc(5)-zc(4)+zc(3)+zc(2)-zc(1))/8.d0
!      write(*,*)'in sub - h'
!      write(*,'(8f5.1)')h0,h1,h2,h3,h4,h5,h6,h7
   
     FuncVec(1) = f0 + f1*CoordL_itr(1) + f2*CoordL_itr(2) + f3*CoordL_itr(3) +       &
                f4*CoordL_itr(1)*CoordL_itr(2) + f5*CoordL_itr(1)*CoordL_itr(3) +     &
                f6*CoordL_itr(2)*CoordL_itr(3) + f7*CoordL_itr(1)*CoordL_itr(2)*CoordL_itr(3)
     FuncVec(2) = g0 + g1*CoordL_itr(1) + g2*CoordL_itr(2) + g3*CoordL_itr(3) +       &
                g4*CoordL_itr(1)*CoordL_itr(2) + g5*CoordL_itr(1)*CoordL_itr(3) +     &
                g6*CoordL_itr(2)*CoordL_itr(3) + g7*CoordL_itr(1)*CoordL_itr(2)*CoordL_itr(3)
     FuncVec(3) = h0 + h1*CoordL_itr(1) + h2*CoordL_itr(2) + h3*CoordL_itr(3) +       &
                h4*CoordL_itr(1)*CoordL_itr(2) + h5*CoordL_itr(1)*CoordL_itr(3) +     &
                h6*CoordL_itr(2)*CoordL_itr(3) + h7*CoordL_itr(1)*CoordL_itr(2)*CoordL_itr(3)
   
    Gmat(1,1) = f1+f4*CoordL_itr(2)+f5*CoordL_itr(3)+f7*CoordL_itr(2)*CoordL_itr(3) !DfDxi
    Gmat(1,2) = f2+f4*CoordL_itr(1)+f6*CoordL_itr(3)+f7*CoordL_itr(1)*CoordL_itr(3) !DfDeta
    Gmat(1,3) = f3+f5*CoordL_itr(1)+f6*CoordL_itr(2)+f7*CoordL_itr(1)*CoordL_itr(2) !DfDkappa
    Gmat(2,1) = g1+g4*CoordL_itr(2)+g5*CoordL_itr(3)+g7*CoordL_itr(2)*CoordL_itr(3) !DgDxi
    Gmat(2,2) = g2+g4*CoordL_itr(1)+g6*CoordL_itr(3)+g7*CoordL_itr(1)*CoordL_itr(3) !DgDeta
    Gmat(2,3) = g3+g5*CoordL_itr(1)+g6*CoordL_itr(2)+g7*CoordL_itr(1)*CoordL_itr(2) !DgDkappa
    Gmat(3,1) = h1+h4*CoordL_itr(2)+h5*CoordL_itr(3)+h7*CoordL_itr(2)*CoordL_itr(3) !DhDxi
    Gmat(3,2) = h2+h4*CoordL_itr(1)+h6*CoordL_itr(3)+h7*CoordL_itr(1)*CoordL_itr(3) !DhDeta
    Gmat(3,3) = h3+h5*CoordL_itr(1)+h6*CoordL_itr(2)+h7*CoordL_itr(1)*CoordL_itr(2) !DhDkappa
!    write(*,*)'Gmat'
!    write(*,*)((Gmat(i,j),j=1,3),i=1,3)
    call invert33(Gmat,Gmatinv)
!     write(*,*)'GmatInv'
!     write(*,*)((Gmatinv(i,j),j=1,3),i=1,3)
    deltaCoordL_itr(1) = dot_product(Gmatinv(1,1:3),-1.d0*FuncVec(1:3))
    deltaCoordL_itr(2) = dot_product(Gmatinv(2,1:3),-1.d0*FuncVec(1:3))
    deltaCoordL_itr(3) = dot_product(Gmatinv(3,1:3),-1.d0*FuncVec(1:3))
!     write(*,*)'deltaCoord'
!     write(*,*)deltaCoordL_itr(1:3)
    CoordL_itr(1:3) = CoordL_itr(1:3) + deltaCoordL_itr(1:3)
    
    errF = sum(abs(FuncVec(1:3)))
    errC = sum(abs(deltaCoordL_itr(1:3)))
    
    if ((errF < TOL) .or. (errC < TOL) .or. (itr .ge. maxitr)) continueItr = .false.
  enddo
  
  NaturalCoords(1:3) = CoordL_itr(1:3)
  ShapeFuncVec(1) = (1.d0-NaturalCoords(1))*(1.d0-NaturalCoords(2))*(1.d0-NaturalCoords(3))/8.d0
  ShapeFuncVec(2) = (1.d0+NaturalCoords(1))*(1.d0-NaturalCoords(2))*(1.d0-NaturalCoords(3))/8.d0
  ShapeFuncVec(3) = (1.d0+NaturalCoords(1))*(1.d0+NaturalCoords(2))*(1.d0-NaturalCoords(3))/8.d0
  ShapeFuncVec(4) = (1.d0-NaturalCoords(1))*(1.d0+NaturalCoords(2))*(1.d0-NaturalCoords(3))/8.d0
  ShapeFuncVec(5) = (1.d0-NaturalCoords(1))*(1.d0-NaturalCoords(2))*(1.d0+NaturalCoords(3))/8.d0
  ShapeFuncVec(6) = (1.d0+NaturalCoords(1))*(1.d0-NaturalCoords(2))*(1.d0+NaturalCoords(3))/8.d0
  ShapeFuncVec(7) = (1.d0+NaturalCoords(1))*(1.d0+NaturalCoords(2))*(1.d0+NaturalCoords(3))/8.d0
  ShapeFuncVec(8) = (1.d0-NaturalCoords(1))*(1.d0+NaturalCoords(2))*(1.d0+NaturalCoords(3))/8.d0

 return
 End Subroutine calcNaturalCoordsHex



 logical function AtomOnNode(AtomCoord,NodeCoord)
!---------------------------------------
!   function AtomOnNode
!      - checks if an atom is coincident with a node
!---------------------------------------
 implicit none
 real*8 AtomCoord(3),NodeCoord(3)
 
 AtomOnNode = .false. 
 if (abs(AtomCoord(1)-NodeCoord(1))<1.e-5 .and. &
     abs(AtomCoord(2)-NodeCoord(2))<1.e-5 .and. &
     abs(AtomCoord(1)-NodeCoord(1))<1.e-5) &
   AtomOnNode = .true.
 
 return
 End function AtomOnNode
 
 Subroutine calcDisp(ElementNodalDisp,ShapeFuncVec,AtomDisp,elemType)
 implicit none
!  real*8, intent(in), dimension(4,3) :: ElementNodalDisp
!  real*8, intent(in), dimension(4) :: ShapeFuncVec
 real*8, intent(in) :: ElementNodalDisp(:,:)
 real*8, intent(in) :: ShapeFuncVec(:)
 real*8, intent(out), dimension(3) :: AtomDisp
 character, intent(in) :: elemType
 
 AtomDisp(1) = dot_product(ShapeFuncVec(1:4),ElementNodalDisp(1:4,1))
 AtomDisp(2) = dot_product(ShapeFuncVec(1:4),ElementNodalDisp(1:4,2))
 AtomDisp(3) = dot_product(ShapeFuncVec(1:4),ElementNodalDisp(1:4,3))
 if (elemType=='H' .or. elemType=='h') then
   AtomDisp(1) = AtomDisp(1)+ dot_product(ShapeFuncVec(5:8),ElementNodalDisp(5:8,1))
   AtomDisp(2) = AtomDisp(2)+ dot_product(ShapeFuncVec(5:8),ElementNodalDisp(5:8,2))
   AtomDisp(3) = AtomDisp(3)+ dot_product(ShapeFuncVec(5:8),ElementNodalDisp(5:8,3))
 endif
 
 return
 End subroutine calcDisp


END MODULE MAPPING

!	/////////////////////////////////////////	
!	/                                    	/
!	/ MAIN PROGRAM 'FE2ATmap'		/
!	/  					/
!	/////////////////////////////////////////

PROGRAM FE2ATmap
  USE FE2ATsize
  USE POLO
  USE MAPPING
  USE qsort_c_module
  implicit none

  ! Definition of integer variables:  
  character fname*80
  character elemType*2
  integer nColsInFile,nCommentLines,nElements,nNodes,nAtoms
  integer nNodalCols,nElemCols,nAtomCols
  integer nNodesWithFEresult,id,nid,minid,maxid,lastid
  integer atomNum,atomType,nloc(3)
  integer nNPE,maxNeighbors,i,j,k
  integer t1,t2,t3,t4,benchT,el
  integer clock_rate,clock_max
  integer, dimension(2), target :: dims
  integer, pointer :: nRows,nCols,first
  integer, allocatable, dimension(:,:) :: Neighbors
  integer, allocatable, dimension(:) :: searchlist

  ! Parameters for point-in-polyhedron testing (tetrahedron or hexahedron):
  integer :: dim_num = 3                                     ! spatial dimension, three what else?!
  integer :: face_num                                        ! number of faces (4 for tetrahedron, 6 for hexahedron)
  integer :: face_order_max                                  ! max. number of nodes per face (3 for tet, 4 for hex)
  integer :: node_num                                        ! number of nodes per element (4 for tet, 8 for hex) 
  integer, allocatable, dimension(:) :: face_order           ! order of each face ( (/ 3, 3, 3, 3 /) for tet,
                                                             ! (/ 4, 4, 4, 4, 4, 4, 4, 4 /) for hex )
  integer, allocatable, dimension(:,:) :: face_point         ! the points making each face (IMPORTANT: must be clockwise!!!)
  real*8, allocatable, dimension(:,:) :: v
  ! Definition of real variables: 
  real*8, allocatable, dimension(:,:) :: dataArray
  integer, allocatable, dimension(:,:) :: ElementConnectivity
  integer, allocatable, dimension(:,:), target :: ElementSortedList
  real, allocatable, dimension(:,:) :: ElementCenters
  real, allocatable, dimension(:,:) :: ElementCentersTransp
  real*8, allocatable, dimension(:,:) :: NodalCoords
  real*8, allocatable, dimension(:,:) :: AtomPositions
  real*8, allocatable, dimension(:,:) :: FEresult
  real*8, allocatable, dimension(:,:) :: DistToNodes
  real*8, dimension(3) :: newAtomCoord
  real*8, allocatable, dimension(:,:) :: ElementNodalCoords,ElementNodalDisp
  real*8, dimension(3) :: p0,pA,pB,vecU,vecU1,vecV,vecN,projAtom
  real*8, dimension(3) :: vecQ0,vecQA,vecQB,vecQ
  real*8, dimension(3) :: field0,fieldA,fieldB,atomInterolatedData
  real*8, dimension(3) :: Ax,Ay,Az,pt
  real*8, dimension(3,3) :: Bmatrix,BmatrixInv,Gmatrix,GmatrixInv,box
  real*8 atomMass,atomCoord(3),valmin(3),dcoeff,distAtomToPlane
  real*8 xmin,ymin,zmin,xmax,ymax,zmax
  real*8 dxmin,dymin,dzmin,dxmax,dymax,dzmax
  real*8, dimension(3) :: NaturalCoords,AtomDisp
  real*8, dimension(8) :: ShapeFuncVec
  real*8, dimension(8,3) :: ElemNodalCoords_restructured
  real*8 :: c(4),eps,t3dElements
  ! temporary:
  real*8 :: sector(2),tar(3),cell(2,3),area
  integer :: count_found = 0,count_unfound = 0
  logical :: inside,zero
  integer childtype,ios,startel
  integer, target :: one
  integer :: benchOp = 0
  integer :: atom_int = 0 
  integer :: atom_last = 0
  integer :: lastBenchOp = 0
  real, dimension(3) :: benchAvgPos
  real :: benchTime = 0
  real :: lastBenchTime = 0
  character(len=150), allocatable, dimension(:) :: arg
  character(len=300), dimension(10,10) :: params
  character(len=300) :: paramfile,coordfile,nodefile,elementfile,dispfile,refheader,thickness_3d,outfile
  character (len=300) :: line,nline
  logical :: twod = 0
  
  real versionNum
  
  allocate(dataArray(nDataLinesMAX,nColsInFileMAX))
  one = 1
  benchAvgPos = 0
  versionNum=3.0

! OUTPUT PROGRAM INFORMATION AND COPYRIGHT
  write(*,'(A)')'# /*****************************************************************************\'
  write(*,'(A)')'# *                                                                             *'
  write(*,'(A)')'# *                               - F E 2 A T -                                 *'
  write(*,'(A,f3.1,A)')'# *                              - version ',versionNum,' -                                *'
  write(*,'(A)')'# *                                                                             *'
  write(*,'(A)')'# * Program to map FE displacements onto Atoms                                  *'
  write(*,'(A)')'# * Copyright (C) 2011-2016                                                     *'
  write(*,'(A)')'# * Authors: J.J. Moeller, A. Prakash & E. Bitzek                               *'
  write(*,'(A)')'# * (Friedrich-Alexander-Universitaet Erlangen-Nuernberg (FAU), Germany)        *'
  write(*,'(A)')'# *                                                                             *'
  write(*,'(A)')'# * This program is free software and is licensed under GPL v3                  *'
  write(*,'(A)')'# * You should have received a copy of the GNU General Public License           *'
  write(*,'(A)')'# * along with this program.  If not, see <http://www.gnu.org/licenses/>        *'
  write(*,'(A)')'# *                                                                             *'
  write(*,'(A)')'# * Please visit the wiki of FE2AT for more details                             *'
  write(*,'(A)')'# * https://bitbucket.org/arunpksh/fe2at/wiki/Home                              *'
  write(*,'(A)')'# *                                                                             *'
  write(*,'(A)')'# /*****************************************************************************\'


! READ COMMAND LINE ARGUMENTS
  allocate(arg(iargc()))
  do i=1,iargc()
    call getarg(i, arg(i))
  end do
  do i=1,iargc()
    if (arg(i) .eq. "-p") then
      write(*,'(A)')'read from parameter file', arg(i+1)
      paramfile=arg(i+1)
    end if
  end do

! READ PARAMETERS FROM PARAMETER FILE
  open(unit=500,file=trim(paramfile),status='old')
  i=1
  do
    read (500,'(A)',end=177) line
!    write(*,*) line
    line = trim(adjustl(line(:)))
    if ((line(1:1) /= "#") .AND. (line(1:1) /= "*")) then
      read(line,*) (params(i,j),j=1,2)
!      write(*,'(A)') params(i,1), params(i,2)
      select case (params(i,1))  
        case("coordfile")
          coordfile=params(i,2)
        case("nodefile") 
          nodefile=params(i,2)
        case("elementfile")
          elementfile=params(i,2)
        case("dispfile")
          dispfile=params(i,2)
        case("thickness_3d")
          thickness_3d=params(i,2)
        case("reference_header")
          refheader=params(i,2)
        case("outfile")
          outfile=params(i,2)
      end select
      i=i+1
    end if  
  end do

177 continue

!        select case (nline(:1))                       ! check what is written as first character in line
!          case (45,46,48:57)                                  ! if it's '.','-' or 0...9 in ASCII format,
!          nColsInFile = nColsInFile+1                         ! increase the counter.
!        end select


!-----------------------------------------------------------------------------------  
! Getting filename with atomistic configuration:
!  print*,'Please enter the filename containing the ATOMISTIC POSITIONS (xyz):'
!  read*,fname
  fname = coordfile
!  print*,'***********************************'
!  nColsInFile = 6
  call FileRead(fname,nColsInFile,dataArray,nAtoms)
  nAtomCols = nColsInFile
  write(*,*)'Number of Atoms',nAtoms
  allocate(AtomPositions(nAtoms,nAtomCols))
  AtomPositions(1:nAtoms,1:nAtomCols)=dataArray(1:nAtoms,1:nAtomCols)

  open(unit=500,file=trim(coordfile),status='old')
  i=1
  do
    read (500,'(A)',end=178) line
    line = trim(adjustl(line(:)))
    select case (line(1:2))
      case ("#X")
      read(line(3:),*) (box(1,j),j=1,3)
      write(*,*)box(1,1)

      case ("#Y")
      read(line(3:),*) (box(2,j),j=1,3)
      write(*,*)box(2,2)

      case ("#Z")
      read(line(3:),*) (box(3,j),j=1,3)
      write(*,*)box(3,3)

      case ("#E")
      exit

    end select
  end do
178 continue
      write(*,*)'continuing...'

!-----------------------------------------------------------------------------------  
! Getting filename with nodal coordinates: 
!  print*,'Please enter the filename containing the NODES:'
!  read*,fname
  fname = nodefile
!  print*,'***********************************'
!  nColsInFile = 4
  call FileRead(fname,nColsInFile,dataArray,nNodes)
  write(*,*)'Number of Nodes',nNodes
! handle 2D coordinates:
  if (nColsInFile == 3) then
    ! 2D elements !
!    write(*,*)'First 2D node:',dataArray(1,1:4)
    print*,'Detected 2D coordinates of nodes.'
    twod = .TRUE.
    if (thickness_3d=="") then
      print*,'Please enter 3D element thickness in z:'
      read*,t3dElements
    elseif (thickness_3d=="x") then
      t3dElements=box(1,1)
    elseif (thickness_3d=="y") then
      t3dElements=box(2,2)
    elseif (thickness_3d=="z") then
      t3dElements=box(3,3)
    else
      read(thickness_3d,*) t3dElements
    end if
    write(*,*) 'Thickness of 2d Elements is ',t3dElements 
    nColsInFile=4
    dataArray(1:nNodes,4)=0
    do i=1,nNodes
      dataArray(nNodes+i,1)=nNodes+i
      dataArray(nNodes+i,2:3)=dataArray(i,2:3)
      dataArray(nNodes+i,4)=t3dElements
    end do
!    write(*,*)'First 3D node (zero plane):',dataArray(1,1:4)
!    write(*,*)'First 3D node (thickness plane):',dataArray(nNodes+1,1:4)
!    write(*,*)'Last 3D node (zero plane):',dataArray(nNodes,1:4)
!    write(*,*)'Last 3D node (thickness plane):',dataArray(2*nNodes,1:4)
    nNodes=2*nNodes
  endif

  allocate(NodalCoords(nNodes,nColsInFile))
  allocate(DistToNodes(nNodes,2))
  NodalCoords(1:nNodes,1:nColsInFile)=dataArray(1:nNodes,1:4)
!  write(*,*) 'First node: ', int(NodalCoords(1,:))


!-----------------------------------------------------------------------------------  
! Getting filename with element information:
!  print*,'Please enter the filename containing the ELEMENTS:'
!  read*,fname
  fname = elementfile
!  print*,'***********************************'
  call FileRead(fname,nColsInFile,dataArray,nElements)
  nNPE = nColsInFile - 1
  write(*,*)'Number of Elements',nElements

! handle 2D elements
  if (nColsInFile == 4) then
    ! 2D traingle elements !
!    write(*,*)'First 2D tri element:',dataArray(1,1:4)

  ! still some stuff to do, i think...

  elseif (nColsInFile == 5 .and. twod == .TRUE.) then
    ! 2D cube elements !
!    write(*,*)'First 2D cube element:',dataArray(1,1:5)
!    print*,'Detected 2D cube elements...'
    nColsInFile=9
    do i=1,nNodes
      dataArray(i,6)=dataArray(i,2)+nNodes/2
      dataArray(i,7)=dataArray(i,3)+nNodes/2
      dataArray(i,8)=dataArray(i,4)+nNodes/2
      dataArray(i,9)=dataArray(i,5)+nNodes/2
    end do
!    write(*,*)'First 3D hex element:',dataArray(1,1:9)
    nNPE = nColsInFile - 1
  endif

  allocate(ElementConnectivity(nElements,nColsInFile))
  allocate(ElementSortedList(nColsInFile+1,nElements))
  ElementConnectivity(1:nElements,1:nColsInFile)=dataArray(1:nElements,1:nColsInFile)
! write(*,*) 'First element: ', int(ElementConnectivity(1,:))


  
!-----------------------------------------------------------------------------------  
! Getting filename with nodal displacements from FE calculation:
!  print*,'Please enter the filename containing the FE DISPLACEMENTS:'
!  read*,fname
  fname = dispfile
!  print*,'***********************************'
!  nColsInFile = 4
  call FileRead(fname,nColsInFile,dataArray,nNodesWithFEresult)
  write(*,*)'Number of Nodes with FE results',nNodesWithFEresult

! handle 2D coordinates:
  if (nColsInFile == 3) then
    ! 2D elements !
    write(*,*)'First 2D node:',dataArray(1:2,1:3)
!    print*,'Detected 2D coordinates of nodes. Please enter 3D element thickness in z:'
!    read*,t3dElements
    nColsInFile=4
    dataArray(1:nNodesWithFEresult,4)=0
    do i=1,nNodes
      dataArray(nNodesWithFEresult+i,1)=nNodesWithFEresult+i
      dataArray(nNodesWithFEresult+i,2:3)=dataArray(i,2:3)
      dataArray(nNodesWithFEresult+i,4)=0
    end do
    write(*,*)'First 3D node (zero plane):',dataArray(1,1:4)
    write(*,*)'First 3D node (thickness plane):',dataArray(nNodes+1,1:4)
    write(*,*)'Last 3D node (zero plane):',dataArray(nNodes,1:4)
    write(*,*)'Last 3D node (thickness plane):',dataArray(2*nNodes,1:4)
    nNodesWithFEresult=2*nNodesWithFEresult
  endif

  allocate(FEresult(nNodesWithFEresult,nColsInFile))
  FEresult(1:nNodesWithFEresult,1:nColsInFile) = dataArray(1:nNodesWithFEresult,1:nColsInFile)
!  write(*,*)FEresult
  deallocate(dataArray)

  ! Check if number of nodes in deformed state is consistent with the undeformed state:
  if(nNodesWithFEresult .ne. nNodes) then
    write(*,'(a90)')'*** ERROR *** Number of nodes with FE results does not conform to the nodes in the FE mesh'
    write(*,*)'ERROR: Incorrect number of nodes'
    stop
  endif

!  nCols => dims(2)
!  nRows => dims(1)
!  dims(:)=shape(ElementConnectivity)
!  nNPE = dims(2)-1
  write(*,*)'Number of nodes per element (NPE):',nNPE

  ! Note the clock for benchmarking:
  CALL system_clock ( t1, clock_rate, clock_max )

  ! check whether the elements' shapes are tertrahedra or hexahedra
  if (nNPE == 4) then
    ! the elements' shapes are tetrahedra
    elemType='t'
    maxNeighbors = 14
    face_num = 4
    face_order_max = 3
    node_num = nNPE 
    allocate(face_order(face_num))
    face_order = (/ 3, 3, 3, 3 /)
    allocate(face_point(face_order_max,face_num))
    face_point = reshape ( (/ 1, 2, 4, &
                              1, 3, 2, &
                              1, 4, 3, &
                              2, 3, 4 /), (/ face_order_max, face_num /) )
  else if (nNPE == 8) then
    ! the elements' shapes are hexahedra
    elemType='h'
    maxNeighbors = 26
    face_num = 6
    face_order_max = 4
    node_num = nNPE
    allocate(face_order(face_num))
    face_order = (/ 4, 4, 4, 4, 4, 4, 4, 4 /)
    allocate(face_point(face_order_max,face_num))
    face_point = reshape ( (/ 1, 4, 3, 2, &
                              5, 6, 7, 8, &
                              2, 3, 7, 6, &
                              1, 5, 8, 4, &
                              3, 4, 8, 7, &
                              1, 2, 6, 5 /), (/ face_order_max, face_num /) )
  else
    write(*,*)'ERROR! The elements are of an unknown shape (neither tetrahedra nor hexahedra)!'
    stop
  end if
  write(*,*)'Element type (h = linear hexahedra; t = linear tetrahedra):',elemType

  allocate(ElementCenters(nElements,3))

  CALL system_clock ( t2, clock_rate, clock_max )
  write(*,'(a$)')'Calculating the center of mass for each element...'
  call CalcCenters(ElementConnectivity,ElementCenters,nNPE+1,NodalCoords)
  CALL system_clock ( t3, clock_rate, clock_max )
  write(*,'(a,F8.3,a)'),'done! (took ',dble(t3-t2)/dble(clock_rate),' seconds)'
  
  allocate(ElementCentersTransp(3,nElements))
  ElementCentersTransp = transpose(ElementCenters)

!  CALL system_clock ( t2, clock_rate, clock_max )
!  write(*,'(a$)')'Determination of the neighbors...'
!  allocate(Neighbors(nElements,nElements))
!  call GetNeighbors2 (ElementConnectivity,ElementCentersTransp,Neighbors,nElements)
!  CALL system_clock ( t3, clock_rate, clock_max )
!  write(*,'(a,F8.3,a)'),'done! (took ',dble(t3-t2)/dble(clock_rate),' seconds)'

  xmin=minval(NodalCoords(:,2))
  xmax=maxval(NodalCoords(:,2))
  ymin=minval(NodalCoords(:,3))
  ymax=maxval(NodalCoords(:,3))
  zmin=minval(NodalCoords(:,4))
  zmax=maxval(NodalCoords(:,4))
  cell=reshape((/xmin,xmax,ymin,ymax,zmin,zmax/),(/2,3/))  

  first => one

  ! Build the octree:
  CALL system_clock ( t2, clock_rate, clock_max )
  write(*,'(a$)')'Determination of the octree...'
  call BuildTree(tree,cell,ElementCenters,ElementConnectivity,ElementSortedList,nElements,first)
  CALL system_clock ( t3, clock_rate, clock_max )
  write(*,'(a,F8.3,a)'),'done! (took ',dble(t3-t2)/dble(clock_rate),' seconds)'


! determine box dimensions of mapped configuration:
! 1. keep initial atomistic box dimensions:
! 2. keep initial FE box dimensions:
! 3. modify initial atomistic box dimensions by FE displacements:
! 4. modify initial FE box dimensions by FE displacements:

  select case (refheader)

    case ("at")
      dxmin=0;dymin=0;dzmin=0
      dxmax=abs(box(1,1));dymax=abs(box(2,2));dzmax=abs(box(3,3));

    case ("fe")
      dxmin=abs(xmin);dymin=abs(ymin);dzmin=abs(zmin);
      dxmax=abs(xmax);dymax=abs(ymax);dzmax=abs(zmax);

    case ("dat")
      dxmin=abs(minval(FEresult(:,2)))
      dxmax=abs(maxval(FEresult(:,2)))+abs(box(1,1))
      dymin=abs(minval(FEresult(:,3)))
      dymax=abs(maxval(FEresult(:,3)))+abs(box(2,2))
      dzmin=abs(minval(FEresult(:,4)))
      dzmax=abs(maxval(FEresult(:,4)))+abs(box(3,3))


    case ("dfe")
      dxmin=abs(minval(FEresult(:,2)+NodalCoords(:,2)))
      dxmax=abs(maxval(FEresult(:,2)+NodalCoords(:,2)))
      dymin=abs(minval(FEresult(:,3)+NodalCoords(:,3)))
      dymax=abs(maxval(FEresult(:,3)+NodalCoords(:,3)))
      dzmin=abs(minval(FEresult(:,4)+NodalCoords(:,4)))
      dzmax=abs(maxval(FEresult(:,4)+NodalCoords(:,4)))

  end select


  fname=outfile
  open(unit=501,file=fname)
  write(501,'(a)')'#F A 1 1 1 3 3 1'
  write(501,'(a)')'#C number type mass x y z u_x u_y u_z FE'
  write(501,'(a,3(1X,F16.6))')'#X',dxmax+dxmin,0,0
  write(501,'(a,3(1X,F16.6))')'#Y',0,dymax+dymin,0
  write(501,'(a,3(1X,F16.6))')'#Z',0,0,dzmax+dzmin 
  write(501,'(a,f3.1,a)')'## FE2AT ',versionNum,' output file - needs relaxation'
  write(501,'(a)')'## Program authors: J.J. Moeller, A. Prakash & E. Bitzek'
  write(501,'(a)')'#E'

  fname='FE2ATmap_output_notallocated.xyz'
  open(unit=503,file=fname)
  write(503,'(a)')'#F A 1 1 1 3 0 0'
  write(503,'(a)')'#C number type mass x y z'
  write(503,'(a,3(1X,F16.6))')'#X',xmax,0,0
  write(503,'(a,3(1X,F16.6))')'#Y',0,ymax,0
  write(503,'(a,3(1X,F16.6))')'#Z',0,0,zmax 
  write(503,'(a,f3.1,a)')'## FE2AT ',versionNum,' output file - only unallocated atoms'
  write(503,'(a)')'## Program authors: J.J. Moeller, A. Prakash & E. Bitzek'
  write(503,'(a)')'#E'
  
  fname='FE2ATmap_benchmark_ForRewAlgo.dat'
  open(unit=502,file=fname)
  write(502,'(a)')'#nr_atoms time_tot time_int time_perAtom operations_tot operations_int operations_perAtom x_avg y_avg z_avg'

  allocate(searchlist(nElements))
  allocate(v(dim_num,node_num))
  allocate(ElementNodalCoords(node_num,dim_num))
  allocate(ElementNodalDisp(node_num,dim_num))

  CALL system_clock ( t2, clock_rate, clock_max )
  write(*,'(a$)')'Mapping of the nodal displacements onto the atoms...'
  benchOp = 0
  lastBenchTime = 0
  lastBenchOp = 0

  do j=1,nAtoms
   ! initialize the testing values (inside = ) and the target atom
!    write(*,*)'working at atom Nr. ',j,AtomPositions(j,4:6)
    inside = .FALSE.
    found = .FALSE.
    tar = AtomPositions(j,4:6)

    ! do the first quick search based on the octree algorithm
    call SearchPoint (tree,tar,ElementCenters,id)
!    write(*,*)'nr: ',id
!    write(*,*)'Neighbors:',Neighbors(el,:)
    ! fill the searchlist for the considered atom, the atom itself sits on position 1
!    searchlist(1)=el
!    searchlist(:)=Neighbors(:,startel)
!    write(*,*)'start id = ',ElementSortedList(1,id)
!    write(*,'(10(I5,1X))') Neighbors(:,startel)
    nid = id
    lastid = id
    minid = .FALSE.
    maxid = .FALSE. 
151 if (inside == .FALSE.) then
      do i=1,nElements
!        write(*,*)'i = ',i-1,'nid = ',nid,'id = ',id
!        if (id > nElements) then
!          id = 1
!        else if (id == 0 ) then
!          id = 
!        end if 
        el = ElementSortedList(2,id)
!        write(*,*)'searching in element nr. ',el
        v = 0
        do k=1,node_num
          v (:,k) = NodalCoords(ElementSortedList(k+2,id),2:4)
!          write(*,'(3(F5.2,1X))') v(:,k)
        end do
        ElementNodalCoords = transpose(v)

        ! point-in-polyhedron test, if the atom is 'inside' the element or not:
        if (nNPE == 8) then 
          call checkZeroNodes(v,node_num,el,ElementCenters)
          call polyhedron_contains_point_3d(node_num,face_num,face_order_max,v,face_order,face_point,tar,inside)
        else
          call polyhedron_contains_point_3d(node_num,face_num,face_order_max,v,face_order,face_point,tar,inside)
          call tetrahedron_barycentric_3d ( v, tar, c )
          eps=0.0001
          inside  =  ( 0.0-eps <= c(1) ) .and. ( c(1) <= 1.0+eps ) .and. &
                     ( 0.0-eps <= c(2) ) .and. ( c(2) <= 1.0+eps ) .and. &
                     ( 0.0-eps <= c(3) ) .and. ( c(3) <= 1.0+eps ) .and. &
                     ( 0.0-eps <= c(4) ) .and. ( c(4) <= 1.0+eps ) .and. &
                     ( c(1) + c(2) + c(3) + c(4) <= 1.0+eps )
        end if 

        if (inside == .TRUE.) goto 151

        ! this is a simple forward algorithm:
!        id = id+1

        ! here starts the forward-rewind algorithm:
        if (id == 1) then
          minid = .TRUE.
          id = lastid
        else if (id == nElements) then
          maxid = .TRUE.
          id = lastid
        end if 
        lastid = id
        if (maxid .eq. .TRUE.) then
          id = lastid - 1 
        else if (minid .eq. .TRUE.) then
          id = lastid + 1
        else if (mod(i,2) == 1) then
          id = id + i
        else if (mod(i,2) == 0) then
          id = id - i
        end if

!        nid = nid + 1
        if (id > nElements) then
          id = 1
        end if
      end do

      count_unfound = count_unfound + 1
!      deallocate(searchlist)
      write(*,'(I3,a,3(F5.2,1X),a)')j,': Atomic coordinates (',AtomPositions(j,4:6), ') could not be allocated.'
      write(503,'(I8,1X,I3,1X,F16.8,1X,3(F16.6,1X))')int(AtomPositions(j,1:2)),AtomPositions(j,3:6)
    else if (inside == .TRUE.) then 
      count_found = count_found + 1

      if (elemType == 't') then
        call calcNaturalCoordsTet(ElementNodalCoords,tar,NaturalCoords,ShapeFuncVec)
!       write(*,'(a,3(F5.2,1X))')'Natural coordinates:', NaturalCoords

      else if (elemType == 'h') then
        ElemNodalCoords_restructured(1:2,1:3)=ElementNodalCoords(1:2,1:3)
        ElemNodalCoords_restructured(3,1:3)=ElementNodalCoords(4,1:3)
        ElemNodalCoords_restructured(4,1:3)=ElementNodalCoords(3,1:3)
        ElemNodalCoords_restructured(5:6,1:3)=ElementNodalCoords(5:6,1:3)
        ElemNodalCoords_restructured(7,1:3)=ElementNodalCoords(8,1:3)
        ElemNodalCoords_restructured(8,1:3)=ElementNodalCoords(7,1:3)
        call calcNaturalCoordsHex(ElemNodalCoords_restructured,tar,NaturalCoords,ShapeFuncVec)
      end if

      do k=1,node_num
        ElementNodalDisp(k,:) = FEresult(ElementSortedList(k+2,id),2:4)
!       write(*,'(I5,1X,3(F8.2,1X))') int(ElementConnectivity(el,k+1)), ElementNodalDisp(k,:)
      end do

      call calcDisp(ElementNodalDisp,ShapeFuncVec,AtomDisp,elemType)

!      write(*,'(a,3(F5.2,1X))') 'AtomDisp: ',AtomDisp
      newAtomCoord(1)=AtomPositions(j,4)+AtomDisp(1)
      newAtomCoord(2)=AtomPositions(j,5)+AtomDisp(2)
      newAtomCoord(3)=AtomPositions(j,6)+AtomDisp(3)

      write(501,'(I8,1X,I3,1X,F16.8,1X,6(F16.6,1X),I7)')int(AtomPositions(j,1:2)),AtomPositions(j,3),newAtomCoord(:),AtomDisp(:),el

      ! for benchmarking:

      benchOp = benchOp + i
      ! write(*,'(a,F8.3,a)'),'done! (took ',dble(t3-t2)/dble(clock_rate),' seconds)'
      benchAvgPos(1) = benchAvgPos(1) + AtomPositions(j,4)
      benchAvgPos(2) = benchAvgPos(2) + AtomPositions(j,5)
      benchAvgPos(3) = benchAvgPos(3) + AtomPositions(j,6)
      if (mod(j,1000) == 0 .or. j == nAtoms) then
        CALL system_clock (benchT, clock_rate, clock_max )
        benchTime = dble(benchT-t2)/dble(clock_rate)
        atom_int = j - atom_last
        write(502,'(I10,1X,2(F10.3,1X),(G10.3,1X),2(I12,1X),(F8.2,1X),3(F16.6,1X))') &
              j,benchTime,benchTime-lastBenchTime,(benchTime-lastBenchTime)/atom_int, &
              benchOp,benchOp-lastBenchOp,real(benchOp-lastBenchOp)/atom_int, &
              benchAvgPos(1)/atom_int,benchAvgPos(2)/atom_int,benchAvgPos(3)/atom_int
        lastBenchTime = benchTime
        lastBenchOp = benchOp
        benchAvgPos = 0
        atom_last = j
      end if      

    end if

  end do
  CALL system_clock ( t3, clock_rate, clock_max )
  write(*,'(a,F8.3,a)'),'done! (took ',dble(t3-t2)/dble(clock_rate),' seconds)'

  deallocate(v)
  deallocate(ElementNodalCoords)
  close(501)

  write(*,'(a,I12,a,I12,a,F6.2,a,I8,a)')'Allocated ',count_found ,' atoms from total',nAtoms, &
                                       ' atoms (',real(count_found/nAtoms*100) ,'%) into ',nElements ,' elements.'
  write(*,'(I12,a)')count_unfound,' atoms could not be allocated.'

  ! Calculate the elapsed time for point-location in FE mesh:
  call system_clock ( t2, clock_rate, clock_max )
  write(*,'(a,F8.3,a)'),'Total elapsed Time: ',dble(t2-t1)/dble(clock_rate),' seconds.' 

END PROGRAM FE2ATmap

! test for polyhedron_contains_point_3d --- a tetrahedron!!!! 

subroutine test0825 ( )

  implicit none

  integer ( kind = 4 ), parameter :: dim_num = 3
  integer ( kind = 4 ), parameter :: face_num = 4
  integer ( kind = 4 ), parameter :: face_order_max = 3
  integer ( kind = 4 ), parameter :: node_num = 4
  integer ( kind = 4 ), parameter :: test_num = 10

  real ( kind = 8 ) area
  real ( kind = 8 ) c(dim_num+1)
  integer ( kind = 4 ), dimension(face_num) :: face_order = (/ 3, 3, 3, 3 /)
  integer ( kind = 4 ), dimension (face_order_max,face_num) :: face_point = reshape ( (/ &
    1, 2, 4, &
    1, 3, 2, &
    1, 4, 3, &
    2, 3, 4 /), (/ face_order_max, face_num /) )
  logical inside1
  logical inside2
  real ( kind = 8 ) p(dim_num)
  integer ( kind = 4 ) seed
  integer ( kind = 4 ) test
  real ( kind = 8 ), dimension ( dim_num, node_num ) :: v = reshape ( (/ & 
    0.0D+00, 0.0D+00, 0.0D+00, &
    1.0D+00, 0.0D+00, 0.0D+00, &
    0.0D+00, 1.0D+00, 0.0D+00, &
    0.0D+00, 0.0D+00, 1.0D+00 /), (/ dim_num, node_num /) )

  write ( *, '(a)' ) ' '
  write ( *, '(a)' ) 'TEST0825'
  write ( *, '(a)' ) '  POLYHEDRON_CONTAINS_POINT_3D determines if a point'
  write ( *, '(a)' ) '  is inside a polyhedron.'
  write ( *, '(a)' ) ' '
  write ( *, '(a)' ) '  We test this routine by using a tetrahedron as '
  write ( *, '(a)' ) '  the polyhedron.'
  write ( *, '(a)' ) '  For this shape, an independent check can be made,'
  write ( *, '(a)' ) '  using barycentric coordinates.'
  write ( *, '(a)' ) ' '
  write ( *, '(a)' ) '  We label these checks IN1 and IN2, and '
  write ( *, '(a)' ) '  we expect them to agree.'

  call r8mat_transpose_print ( dim_num, node_num, v, '  The vertices:' )

  call i4vec_print ( face_num, face_order, '  The face orders:' )

  call i4mat_transpose_print ( face_order_max, face_num, face_point, &
    '  The nodes making each face:' )

  write ( *, '(a)' ) ' '
  write ( *, '(a)' ) '      X           Y           Z      IN1 IN2'
  write ( *, '(a)' ) ' '

  seed = 123456789

  do test = 1, test_num
  
    call r8vec_uniform_01 ( dim_num, seed, p )

    call polyhedron_contains_point_3d ( node_num, face_num, &
      face_order_max, v, face_order, face_point, p, inside1 )


    call tetrahedron_barycentric_3d ( v, p, c )

    inside2 =  ( 0.0D+00 <= c(1) ) .and. ( c(1) <= 1.0D+00 ) .and. &
               ( 0.0D+00 <= c(2) ) .and. ( c(2) <= 1.0D+00 ) .and. &
               ( 0.0D+00 <= c(3) ) .and. ( c(3) <= 1.0D+00 ) .and. &
               ( 0.0D+00 <= c(4) ) .and. ( c(4) <= 1.0D+00 ) .and. &
               ( c(1) + c(2) + c(3) + c(4) <= 1.0D+00 )
    write ( *, '(2x,g14.6,2x,g14.6,2x,g14.6,2x,l1,2x,l1)' ) &
      p(1:3), inside1, inside2

    if ( inside1 .neqv. inside2 ) then
      write ( *, '(a)' ) '??? Disagreement!  Barycentric coordinates:'
      write ( *, '(2x,g14.6,2x,g14.6,2x,g14.6,2x,g14.6)' ) c(1:4)
    end if

  end do

  return
end

!*****************************************************************************80
!  STUFF FOR POINT IN POLYHEDRON TEST:
!! POLYHEDRON_CONTAINS_POINT_3D determines if a point is inside a polyhedron.
!
!  Discussion:
!
!    The reference states that the polyhedron should be simple (that
!    is, the faces should form a single connected surface), and that 
!    the individual faces should be consistently oriented.
!
!    However, the polyhedron does not, apparently, need to be convex.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license. 
!
!  Modified:
!
!    30 August 2005
!
!  Author:
!
!    John Burkardt
!
!  Reference:
!
!    Paulo Cezar Pinto Carvalho, Paulo Roma Cavalcanti,
!    Point in Polyhedron Testing Using Spherical Polygons,
!    in Graphics Gems V,
!    edited by Alan Paeth,
!    Academic Press, 1995,
!    ISBN: 0125434553,
!    LC: T385.G6975.
!  Parameters:
!
!    Input, integer ( kind = 4 ) NODE_NUM, the number of vertices.
!
!    Input, integer ( kind = 4 ) FACE_NUM, the number of faces.
!
!    Input, integer ( kind = 4 ) FACE_ORDER_MAX, the maximum order of any face.
!
!    Input, real ( kind = 8 ) V(3,NODE_NUM), the coordinates of the vertices.
!
!    Input, integer ( kind = 4 ) FACE_ORDER(FACE_NUM), the order of each face.
!
!    Input, integer ( kind = 4 ) FACE_POINT(FACE_ORDER_MAX,FACE_NUM), the 
!    indices of the nodes that make up each face.
!
!    Input, real ( kind = 8 ) P(3), the point to be tested.
!
!    Output, logical INSIDE, is true if the point 
!    is inside the polyhedron.
!

subroutine polyhedron_contains_point_3d ( node_num, face_num, &
  face_order_max, v, face_order, face_point, p, inside)

  implicit none

  integer ( kind = 4 ), parameter :: dim_num = 3
  integer ( kind = 4 ) face_num
  integer ( kind = 4 ) face_order_max
  integer ( kind = 4 ) node_num

  real ( kind = 8 ) area
  integer ( kind = 4 ) face
  integer ( kind = 4 ) face_order(face_num)
  integer face_point(face_order_max,face_num)
  logical inside
  integer ( kind = 4 ) k
  integer ( kind = 4 ) node
  integer ( kind = 4 ) node_num_face
  real ( kind = 8 ) p(dim_num)
  real ( kind = 8 ), parameter :: pi = 3.141592653589793D+00
  real ( kind = 8 ) solid_angle
  real ( kind = 8 ) v(dim_num,node_num)
  real ( kind = 8 ) v_face(dim_num,face_order_max)

  area = 0.0D+00

  do face = 1, face_num

    node_num_face = face_order(face)

    do k = 1, node_num_face

      node = face_point(k,face)

      v_face(1:dim_num,k) = v(1:dim_num,node)

    end do

    call polygon_solid_angle_3d ( node_num_face, v_face, p, solid_angle )
    area = area + solid_angle

  end do
!    write(*,*)'solid_angle: ',solid_angle
!    write(*,*)'area: ',area
!
!  AREA should be -4*PI, 0, or 4*PI.
!  So this test should be quite safe!
!
  if ( area < -2.0D+00 * pi .or. 2.0D+00 * pi < area ) then
    inside = .true.
  else
    inside = .false.
  end if

  return
end

subroutine r8vec_cross_product_3d ( v1, v2, v3 )
  implicit none

  real ( kind = 8 ) v1(3)
  real ( kind = 8 ) v2(3)
  real ( kind = 8 ) v3(3)

  v3(1) = v1(2) * v2(3) - v1(3) * v2(2)
  v3(2) = v1(3) * v2(1) - v1(1) * v2(3)
  v3(3) = v1(1) * v2(2) - v1(2) * v2(1)

  return
end

subroutine polygon_solid_angle_3d ( n, v, p, solid_angle )

  implicit none

  integer ( kind = 4 ), parameter :: dim_num = 3
  integer ( kind = 4 ) n

  real ( kind = 8 ) a(dim_num)
  real ( kind = 8 ) angle
  real ( kind = 8 ) arc_cosine
  real ( kind = 8 ) area
  real ( kind = 8 ) b(dim_num)
  real ( kind = 8 ) r8vec_norm
  real ( kind = 8 ) r8vec_scalar_triple_product
  integer ( kind = 4 ) i4_wrap
  integer ( kind = 4 ) j
  integer ( kind = 4 ) jp1
  real ( kind = 8 ) normal1(dim_num)
  real ( kind = 8 ) normal1_norm
  real ( kind = 8 ) normal2(dim_num)
  real ( kind = 8 ) normal2_norm
  real ( kind = 8 ) p(dim_num)
  real ( kind = 8 ), parameter :: pi = 3.141592653589793D+00
  real ( kind = 8 ) plane(dim_num)
  real ( kind = 8 ) r1(dim_num)
  real ( kind = 8 ) s
  real ( kind = 8 ) solid_angle
  real ( kind = 8 ) v(dim_num,n)

  if ( n < 3 ) then
    solid_angle = 0.0D+00
    return
  end if

  call polygon_normal_3d ( n, v, plane )
 
  a(1:dim_num) = v(1:dim_num,n) - v(1:dim_num,1)

  area = 0.0D+00

  do j = 1, n

    r1(1:dim_num) = v(1:dim_num,j) - p(1:dim_num)

    jp1 = i4_wrap ( j + 1, 1, n )

    b(1:dim_num) = v(1:dim_num,jp1) - v(1:dim_num,j)

    call r8vec_cross_product_3d ( a, r1, normal1 )

    normal1_norm = r8vec_norm ( dim_num, normal1 )

    call r8vec_cross_product_3d ( r1, b, normal2 )

    normal2_norm = r8vec_norm ( dim_num, normal2 )
    
    s = dot_product ( normal1(1:dim_num), normal2(1:dim_num) ) &
      / ( normal1_norm * normal2_norm )

    angle = arc_cosine ( s )

    s = r8vec_scalar_triple_product ( b, a, plane )

    if ( 0.0D+00 < s ) then
      area = area + pi - angle
    else
      area = area + pi + angle
    end if

    a(1:dim_num) = -b(1:dim_num)

  end do

  area = area - pi * real ( n - 2, kind = 8 )

  if ( 0.0D+00 < dot_product ( plane(1:dim_num), r1(1:dim_num) ) ) then
    solid_angle = -area
  else
    solid_angle = area
  end if

  return
end

subroutine polygon_normal_3d ( n, v, normal ) 

  implicit none

  integer ( kind = 4 ), parameter :: dim_num = 3
  integer ( kind = 4 ) n

  real ( kind = 8 ) r8vec_norm
  integer ( kind = 4 ) j
  real ( kind = 8 ) normal(dim_num)
  real ( kind = 8 ) normal_norm
  real ( kind = 8 ) p(dim_num)
  real ( kind = 8 ) v(dim_num,n)
  real ( kind = 8 ) v1(dim_num)
  real ( kind = 8 ) v2(dim_num)

  normal(1:dim_num) = 0.0D+00

  v1(1:dim_num) = v(1:dim_num,2) - v(1:dim_num,1)

  do j = 3, n

    v2(1:dim_num) = v(1:dim_num,j) - v(1:dim_num,1)

    call r8vec_cross_product_3d ( v1, v2, p )

    normal(1:dim_num) = normal(1:dim_num) + p(1:dim_num)

    v1(1:dim_num) = v2(1:dim_num)

  end do
!
!  Normalize.
!
  normal_norm = r8vec_norm ( dim_num, normal )

  if ( normal_norm == 0.0D+00 ) then
    return
  end if

  normal(1:dim_num) = normal(1:dim_num) / normal_norm

  return
end

function i4_wrap ( ival, ilo, ihi )
  implicit none

  integer ( kind = 4 ) i4_modp
  integer ( kind = 4 ) i4_wrap
  integer ( kind = 4 ) ihi
  integer ( kind = 4 ) ilo
  integer ( kind = 4 ) ival
  integer ( kind = 4 ) jhi
  integer ( kind = 4 ) jlo
  integer ( kind = 4 ) wide

  jlo = min ( ilo, ihi )
  jhi = max ( ilo, ihi )

  wide = jhi - jlo + 1

  if ( wide == 1 ) then
    i4_wrap = jlo
  else
    i4_wrap = jlo + i4_modp ( ival - jlo, wide )
  end if

  return
end

function r8vec_norm ( n, a ) 
  implicit none

  integer ( kind = 4 ) n

  real ( kind = 8 ) a(n)
  real ( kind = 8 ) r8vec_norm

  r8vec_norm = sqrt ( sum ( a(1:n)**2 ) )

  return
end

function arc_cosine ( c )
  implicit none

  real ( kind = 8 ) arc_cosine
  real ( kind = 8 ) c
  real ( kind = 8 ) c2

  c2 = c
  c2 = max ( c2, -1.0D+00 )
  c2 = min ( c2, +1.0D+00 )

  arc_cosine = acos ( c2 )

  return
end

function r8vec_scalar_triple_product ( v1, v2, v3 )
  implicit none

  real ( kind = 8 ) r8vec_scalar_triple_product
  real ( kind = 8 ) v1(3)
  real ( kind = 8 ) v2(3)
  real ( kind = 8 ) v3(3)
  real ( kind = 8 ) v4(3)

  call r8vec_cross_product_3d ( v2, v3, v4 )

  r8vec_scalar_triple_product = dot_product ( v1(1:3), v4(1:3) )

  return
end

function i4_modp ( i, j )
  implicit none

  integer ( kind = 4 ) i
  integer ( kind = 4 ) i4_modp
  integer ( kind = 4 ) j

  if ( j == 0 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'I4_MODP - Fatal error!'
    write ( *, '(a,i8)' ) '  I4_MODP ( I, J ) called with J = ', j
    stop
  end if

  i4_modp = mod ( i, j )

  if ( i4_modp < 0 ) then
    i4_modp = i4_modp + abs ( j )
  end if

  return
end

subroutine r8mat_transpose_print ( m, n, a, title )
  implicit none

  integer ( kind = 4 ) m
  integer ( kind = 4 ) n

  real ( kind = 8 ) a(m,n)
  character ( len = * ) title

  call r8mat_transpose_print_some ( m, n, a, 1, 1, m, n, title )

  return
end

subroutine i4vec_print ( n, a, title )
  implicit none

  integer ( kind = 4 ) n

  integer ( kind = 4 ) a(n)
  integer ( kind = 4 ) i
  character ( len = * ) title

  write ( *, '(a)' ) ' '
  write ( *, '(a)' ) trim ( title )
  write ( *, '(a)' ) ' '
  do i = 1, n
    write ( *, '(2x,i8,a,2x,i12)' ) i, ':', a(i)
  end do

  return
end

subroutine i4mat_transpose_print ( m, n, a, title )
  implicit none

  integer ( kind = 4 ) m
  integer ( kind = 4 ) n

  integer ( kind = 4 ) a(m,n)
  character ( len = * ) title

  call i4mat_transpose_print_some ( m, n, a, 1, 1, m, n, title )

  return
end

subroutine r8vec_uniform_01 ( n, seed, r )
  implicit none

  integer ( kind = 4 ) n

  integer ( kind = 4 ) i
  integer ( kind = 4 ) k
  integer ( kind = 4 ) seed
  real ( kind = 8 ) r(n)

  if ( seed == 0 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'R8VEC_UNIFORM_01 - Fatal error!'
    write ( *, '(a)' ) '  Input value of SEED = 0.'
    stop
  end if

  do i = 1, n

    k = seed / 127773

    seed = 16807 * ( seed - k * 127773 ) - k * 2836

    if ( seed < 0 ) then
      seed = seed + 2147483647
    end if

    r(i) = real ( seed, kind = 8 ) * 4.656612875D-10

  end do

  return
end

subroutine tetrahedron_barycentric_3d ( tetra, p, c )
  implicit none

  integer ( kind = 4 ), parameter :: dim_num = 3
  integer ( kind = 4 ), parameter :: rhs_num = 1

  real ( kind = 8 ) a(dim_num,dim_num+rhs_num)
  real ( kind = 8 ) c(dim_num+1)
  integer ( kind = 4 ) i
  integer ( kind = 4 ) info
  real ( kind = 8 ) p(dim_num)
  real ( kind = 8 ) tetra(dim_num,4)
!
!  Set up the linear system
!
!    ( X2-X1  X3-X1  X4-X1 ) C2    X - X1
!    ( Y2-Y1  Y3-Y1  Y4-Y1 ) C3  = Y - Y1
!    ( Z2-Z1  Z3-Z1  Z4-Z1 ) C4    Z - Z1
!
!  which is satisfied by the barycentric coordinates of P.
!
  a(1:dim_num,1:3) = tetra(1:dim_num,2:4)
  a(1:dim_num,4) = p(1:dim_num)

  do i = 1, dim_num
    a(i,1:4) = a(i,1:4) - tetra(i,1)
  end do
!
!  Solve the linear system.
!
  call r8mat_solve ( dim_num, rhs_num, a, info )

  if ( info /= 0 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'TETRAHEDRON_BARYCENTRIC_3D - Fatal error!'
    write ( *, '(a)' ) '  The linear system is singular.'
    write ( *, '(a)' ) '  The input data does not form a proper tetrahedron.'
    stop
  end if

  c(2:4) = a(1:dim_num,4)

  c(1) = 1.0D+00 - sum ( c(2:4) )

  return
end
subroutine r8mat_transpose_print_some ( m, n, a, ilo, jlo, ihi, jhi, title )
  implicit none

  integer ( kind = 4 ), parameter :: incx = 5
  integer ( kind = 4 ) m
  integer ( kind = 4 ) n

  real ( kind = 8 ) a(m,n)
  character ( len = 14 ) ctemp(incx)
  integer ( kind = 4 ) i
  integer ( kind = 4 ) i2
  integer ( kind = 4 ) i2hi
  integer ( kind = 4 ) i2lo
  integer ( kind = 4 ) ihi
  integer ( kind = 4 ) ilo
  integer ( kind = 4 ) inc
  integer ( kind = 4 ) j
  integer ( kind = 4 ) j2hi
  integer ( kind = 4 ) j2lo
  integer ( kind = 4 ) jhi
  integer ( kind = 4 ) jlo
  character ( len = * )  title

  write ( *, '(a)' ) ' '
  write ( *, '(a)' ) trim ( title )

  do i2lo = max ( ilo, 1 ), min ( ihi, m ), incx

    i2hi = i2lo + incx - 1
    i2hi = min ( i2hi, m )
    i2hi = min ( i2hi, ihi )

    inc = i2hi + 1 - i2lo

    write ( *, '(a)' ) ' '

    do i = i2lo, i2hi
      i2 = i + 1 - i2lo
      write ( ctemp(i2), '(i8,6x)' ) i
    end do

    write ( *, '(''  Row   '',5a14)' ) ctemp(1:inc)
    write ( *, '(a)' ) '  Col'
    write ( *, '(a)' ) ' '

    j2lo = max ( jlo, 1 )
    j2hi = min ( jhi, n )

    do j = j2lo, j2hi

      do i2 = 1, inc
        i = i2lo - 1 + i2
        write ( ctemp(i2), '(g14.6)' ) a(i,j)
      end do

      write ( *, '(i5,a,5a14)' ) j, ':', ( ctemp(i), i = 1, inc )

    end do

  end do

  return
end

subroutine i4mat_transpose_print_some ( m, n, a, ilo, jlo, ihi, jhi, title )
  implicit none

  integer ( kind = 4 ), parameter :: incx = 10
  integer ( kind = 4 ) m
  integer ( kind = 4 ) n

  integer ( kind = 4 ) a(m,n)
  character ( len = 8 )  ctemp(incx)
  integer ( kind = 4 ) i
  integer ( kind = 4 ) i2
  integer ( kind = 4 ) i2hi
  integer ( kind = 4 ) i2lo
  integer ( kind = 4 ) ihi
  integer ( kind = 4 ) ilo
  integer ( kind = 4 ) inc
  integer ( kind = 4 ) j
  integer ( kind = 4 ) j2hi
  integer ( kind = 4 ) j2lo
  integer ( kind = 4 ) jhi
  integer ( kind = 4 ) jlo
  character ( len = * )  title

  write ( *, '(a)' ) ' '
  write ( *, '(a)' ) trim ( title )

  do i2lo = max ( ilo, 1 ), min ( ihi, m ), incx

    i2hi = i2lo + incx - 1
    i2hi = min ( i2hi, m )
    i2hi = min ( i2hi, ihi )

    inc = i2hi + 1 - i2lo

    write ( *, '(a)' ) ' '

    do i = i2lo, i2hi
      i2 = i + 1 - i2lo
      write ( ctemp(i2), '(i8)' ) i
    end do

    write ( *, '(''  Row '',10a8)' ) ctemp(1:inc)
    write ( *, '(a)' ) '  Col'
    write ( *, '(a)' ) ' '

    j2lo = max ( jlo, 1 )
    j2hi = min ( jhi, n )

    do j = j2lo, j2hi

      do i2 = 1, inc

        i = i2lo - 1 + i2

        write ( ctemp(i2), '(i8)' ) a(i,j)

      end do

      write ( *, '(i5,a,10a8)' ) j, ':', ( ctemp(i), i = 1, inc )

    end do

  end do

  return
end

subroutine r8mat_solve ( n, rhs_num, a, info )
  implicit none

  integer ( kind = 4 ) n
  integer ( kind = 4 ) rhs_num

  real ( kind = 8 ) a(n,n+rhs_num)
  real ( kind = 8 ) apivot
  real ( kind = 8 ) factor
  integer ( kind = 4 ) i
  integer ( kind = 4 ) info
  integer ( kind = 4 ) ipivot
  integer ( kind = 4 ) j

  info = 0

  do j = 1, n
!
!  Choose a pivot row.
!
    ipivot = j
    apivot = a(j,j)

    do i = j+1, n
      if ( abs ( apivot ) < abs ( a(i,j) ) ) then
        apivot = a(i,j)
        ipivot = i
      end if
    end do

    if ( apivot == 0.0D+00 ) then
      info = j
      return
    end if
!
!  Interchange.
!
    do i = 1, n + rhs_num
      call r8_swap ( a(ipivot,i), a(j,i) )
    end do
!
!  A(J,J) becomes 1.
!
    a(j,j) = 1.0D+00
    a(j,j+1:n+rhs_num) = a(j,j+1:n+rhs_num) / apivot
!
!  A(I,J) becomes 0.
!
    do i = 1, n

      if ( i /= j ) then

        factor = a(i,j)
        a(i,j) = 0.0D+00
        a(i,j+1:n+rhs_num) = a(i,j+1:n+rhs_num) - factor * a(j,j+1:n+rhs_num)

      end if

    end do

  end do

  return
end

subroutine r8_swap ( x, y )
  implicit none

  real ( kind = 8 ) x
  real ( kind = 8 ) y
  real ( kind = 8 ) z

  z = x
  x = y
  y = z

  return
end
