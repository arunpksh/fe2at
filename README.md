            _______  _______  ______  _______  _______             
           |    ___||    ___||__    ||   _   ||_     _|            
           |    ___||    ___||    __||       |  |   |              
           |___|    |_______||______||___|___|  |___|              
                                                                   
 Mapping the nodal displacements of a deformed FE mesh onto atoms  
                                                                   
 Current Version: 3.0                                              
 Date: 2016-03-31                                                  
 Written by: J.J. Moeller, A. Prakash                              
 Friedrich-Alexander-Universitaet Erlangen-Nuernberg,              
 Department Materials Science and Engineering,                     
 Institute I: General Materials Properties (WWI),                  
 Martensstr. 5, D-91058 Erlangen, Germany                           
 http://www.gmp.ww.uni-erlangen.de/                                
                                                                   
 E-Mail contact:                                                   
 fe2at@ww.uni-erlangen.de                                          
                                                                   
 Please cite FE2AT:                                                
 J.J. Möller, A. Prakash, E. Bitzek,                               
 FE2AT - finite element informed atomistic simulations             
 Modelling Simul. Mater. Sci. Eng. 21 (2013) 055011               
 http://dx.doi.org/10.1088/0965-0393/21/5/055011 
                                                                   
